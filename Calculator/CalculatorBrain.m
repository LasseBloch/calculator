//
//  CalculatorBrain.m
//  Calculator
//
//  Created by Lasse Bloch Lauritsen on 06/10/12.
//  Copyright (c) 2012 Lasse Bloch Lauritsen. All rights reserved.
//

#import "CalculatorBrain.h"
#import <math.h>

@interface CalculatorBrain()
@property (nonatomic, strong) NSMutableArray *programStack;


@end

@implementation CalculatorBrain

@synthesize programStack = _programStack;

- (NSMutableArray *)programStack
{
    // We are lazy
    if(_programStack == nil)
    {
        _programStack = [[NSMutableArray alloc] init];
    }
    return _programStack;
}


+ (NSSet *)getSingleOperandSet
{
    // this have to be bad code
    static NSSet *singleOperandSet = nil;
    if (singleOperandSet == nil) {
        // initialize with sqrt string to test with
        singleOperandSet = [[NSSet alloc] initWithObjects:(NSString *)@"sqrt", nil];
    }
    return singleOperandSet;
}

- (void)pushOperand:(double)operand
{
    // A mutableArray can only add obj's so convert the operand to an object (NSNumber)
    [self.programStack addObject:[NSNumber numberWithDouble:operand]];
    NSLog(@"pushOperand Stack is %@", [self.programStack description]);
}



-(double)performOperation:(NSString *)operation
{
    [self.programStack addObject:operation];
    return [CalculatorBrain runProgram:self.program];
}

// getter for program (read only property)
- (id)program
{
    // We wont return our own program stack (internal state), so we make a "Emutable" copy
    return [self.programStack copy];
}

+(NSString *) descriptionOfProgram:(id)program
{
    NSString* result;
    // test of NSet
    NSSet *testSet = [self getSingleOperandSet];
    
    if([testSet containsObject:[NSString stringWithFormat:@"sqrt"]])
    {
        NSLog(@"yay sqrt was found in set: %@", testSet);
    }
    
    
    return result;
}


+ (double) popOperandOffStack:(NSMutableArray *)stack
{
    double result = 0;
    
    id topOffStack = [stack lastObject];
    if(topOffStack)
    {
        [stack removeLastObject];
    }
    
    if ([topOffStack isKindOfClass:[NSNumber class]])
    {
        result = [topOffStack doubleValue];
    }else if( [topOffStack isKindOfClass:[NSString class]] ) {
        NSString *operation = topOffStack;
        // Note some calculations uses a and b
        // this is because when we pop from the stack
        // b and then a if ex the user presses
        // 5 enter 2 enter - he expects 3 (a - b = 3) and not b - a
        if([operation isEqualToString:@"+"])
        {
            result = [self popOperandOffStack:stack] + [self popOperandOffStack:stack];
        }else if([operation isEqualToString:@"*"])
        {
            result = [self popOperandOffStack:stack] * [self popOperandOffStack:stack];
        }else if([operation isEqualToString:@"/"])
        {
            double b = [self popOperandOffStack:stack];
            double a = [self popOperandOffStack:stack];
            if(b == 0.0)
            {
                result = 0;
            }else
            {
                result = a / b;
            }
        }else if([operation isEqualToString:@"-"])
        {
            double b = [self popOperandOffStack:stack];
            double a = [self popOperandOffStack:stack];
            result = a - b;
        }else if([operation isEqualToString:@"sin"])
        {
            result = sin([self popOperandOffStack:stack]);
        }else if([operation isEqualToString:@"cos"])
        {
            result = cos([self popOperandOffStack:stack]);
        }else if ([operation isEqualToString:@"sqrt"])
        {
            result = sqrt([self popOperandOffStack:stack]);
        }else if([operation isEqualToString:@"π"])
        {
            result = 3.141592653589793;
        }else if([operation isEqualToString:@"C"])
        {
            [self clearProgram:stack];
        }
    }
     NSLog(@"popOperandOffStack Stack is %@", [stack description]);
    return result;
}

+(double) runProgram:(id)program
{
    // create a mutableArray as stack
    NSMutableArray *stack;
    // introspection (Getting information about obj's at runtime) it's awsome!
    // We check if program is a instance of NSArray or of a class that inherids form it
    if([program isKindOfClass:[NSArray class]])
    {
        // make a copy of the program so "the uses dont f**** with while we are
        // preforming operations on it, we want a mutable copy so we can munch on it
        stack = [program mutableCopy];
    }
    return [self popOperandOffStack:stack];
    
}


// Clears the program stack
+(void)clearProgram:(NSMutableArray *)stack
{
    if(stack)
    {
        [stack removeAllObjects];
    }
    // run tests
    [self descriptionOfProgram:(nil)];
}

@end
