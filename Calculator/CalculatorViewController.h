//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Lasse Bloch Lauritsen on 06/10/12.
//  Copyright (c) 2012 Lasse Bloch Lauritsen. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CalculatorViewController : UIViewController
// Outlet weak binding because if it is not in the display  ("displayed")
// We have no need to maintain a poiter to it 
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (weak, nonatomic) IBOutlet UILabel *displayHistory;

@end 
