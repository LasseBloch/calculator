//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Lasse Bloch Lauritsen on 06/10/12.
//  Copyright (c) 2012 Lasse Bloch Lauritsen. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
@property (nonatomic) BOOL userIsInTheMiddleOfEnteringANumber;
@property (nonatomic, strong) CalculatorBrain *brain;
@end

@implementation CalculatorViewController

@synthesize display = _display;
@synthesize displayHistory = _displayHistory;
@synthesize userIsInTheMiddleOfEnteringANumber = _userIsInTheMiddleOfEnteringANumber;
@synthesize brain = _brain;

- (CalculatorBrain *)brain
{
    if (!_brain) {
        _brain = [[CalculatorBrain alloc] init];
    }
    return _brain;
}

- (void)setHistoryDisplay:(NSString *)textToAdd
{
    // Get the current history
    NSString *tempText = self.displayHistory.text;
    NSUInteger MAXLENGHT = 35;
    
    tempText = [tempText stringByAppendingString:textToAdd];
    
    // speciel case if C (Clear was pressed)
    if([textToAdd isEqualToString:@"C "])
    {
        tempText = @"";
    }
    
    if(tempText.length > MAXLENGHT)
    {
        // substring the string to be 30 long then chop it the first " "
        // So we throw out whole numbers operands
        tempText = [tempText substringFromIndex:(tempText.length - MAXLENGHT)];
        NSRange range = [tempText rangeOfString:@" "];
        if(range.location == NSNotFound)
        {
            // We wont try to display a number with more than MAXLENGHT digits in history
            return;
        }
       
        tempText = [tempText substringFromIndex:range.location];

    }
    
self.displayHistory.text = tempText;

}

- (IBAction)digitPressed:(UIButton *)sender
{

    NSString *digit = [sender currentTitle];
    
    if(self.userIsInTheMiddleOfEnteringANumber)
    {
        // to prevent leading zeros when display is saying "0" only
        if([self.display.text isEqualToString:@"0"])
        {
            // not nessesary but is more readable
            self.display.text = digit;
        }else
        {
            self.display.text = [self.display.text stringByAppendingString:digit];
        }
        
    } else {
        self.display.text = digit;
        self.userIsInTheMiddleOfEnteringANumber = YES;
    }
}

- (IBAction)operationPressed:(UIButton *)sender
{
    if(self.userIsInTheMiddleOfEnteringANumber)
    {
        [self enterPressed];
    }
    double result = [self.brain performOperation:sender.currentTitle];
    NSString *resultString = [NSString stringWithFormat:@"%g", result];
    self.display.text = resultString;
    // add operand to history
    NSString *historyString = [NSString stringWithFormat:@"%@ ", sender.currentTitle];
    //self.displayHistory.text = [self.displayHistory.text stringByAppendingString:historyString];
    [self setHistoryDisplay:historyString];
}

- (IBAction)enterPressed
{
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInTheMiddleOfEnteringANumber = NO;
    // add number to history
    NSString *historyString = [NSString stringWithFormat:@"%@ ", self.display.text];
    //self.displayHistory.text = [self.displayHistory.text stringByAppendingString:historyString];
    [self setHistoryDisplay:historyString];
    // clear display after entring of number
    self.display.text = @"0";
}

- (IBAction)decimalPointPressed:(UIButton *)sender
{
    // Look for the first occurence of "."
    NSRange range = [self.display.text rangeOfString:@"."];
    // if there was no "." in the display we can add one
    if(range.location == NSNotFound)
    {
        self.userIsInTheMiddleOfEnteringANumber = TRUE;
        self.display.text = [self.display.text stringByAppendingString:@"."];
    }
}

//Removes the last typed decimal
- (IBAction)deleteDecimal:(UIButton *)sender {
    if(self.display.text.length > 0)
    {
        // there is more than 1 char delete the last
        if(self.display.text.length > 1)
           {
               // Yo dawg i've heard you like method calls, so i've put method calls in you method calls
               self.display.text = [self.display.text substringToIndex:self.display.text.length - 1];
           }else if(self.display.text.length == 1 && ![self.display.text isEqualToString:@"0"]) 
           //if we try to delete the last char change it to 0 unless it already is
           {
               self.display.text = @"0";
           }
    }
}

- (void)viewDidUnload {
    [self setDisplayHistory:nil];
    [super viewDidUnload];
}
@end
