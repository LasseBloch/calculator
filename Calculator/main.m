//
//  main.m
//  Calculator
//
//  Created by Lasse Bloch Lauritsen on 06/10/12.
//  Copyright (c) 2012 Lasse Bloch Lauritsen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalculatorAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CalculatorAppDelegate class]));
    }
}
