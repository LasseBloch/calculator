//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Lasse Bloch Lauritsen on 06/10/12.
//  Copyright (c) 2012 Lasse Bloch Lauritsen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalculatorBrain : NSObject

//Two public methods (Because they are in the header file)
- (void)pushOperand:(double)operand;
-(double)performOperation:(NSString *)operation;

@property (nonatomic ,readonly) id program;
// set containg all single opperand oberators like sqrt
// This is properly bad obj c style not using a property (this is not guarded in any way)
+ (NSSet *)singleOperandSet;

// class methods +
// instead of makeing a program class we uses id (kinda like obj in java)
+ (NSString *) descriptionOfProgram:(id) program;
+ (double) runProgram:(id) program;
+ (double) runProgramUsingVariables:(NSDictionary *)variableValues;
+ (NSSet *) variablesUsedInProgram;

@end
